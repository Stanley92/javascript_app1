// alert('This works!');

const defaultResult = 0 ; 
let currentResult = defaultResult
let logEntries = [];

// extracts user input from input field
function getUserNumberInput() {
    return parseInt(userInput.value)
}

// outputs calculation to user
function createAndWriteOutput(operator, resultBeforeCalc, calcNumber){
    const calcDescription = `${resultBeforeCalc} ${operator} ${calcNumber}`
    outputResult(currentResult, calcDescription) // from vendor file
}

function writeToLOg(
    operationIdentifier,
    prevResult,
    operationNumber,
    newResult
){
    const logEntry = {
        operation: operationIdentifier,
        prevResult: prevResult,
        number: operationNumber,
        result: newResult
    }; //object
    console.log(logEntry)
}

function add(){
    const enteredNumber = getUserNumberInput();
    const initialResult = currentResult
    currentResult += enteredNumber ;
    createAndWriteOutput('+', initialResult, enteredNumber)
    writeToLOg('ADD', initialResult, enteredNumber, currentResult)
}

function substract() {
    const enteredNumber = getUserNumberInput();
    const initialResult = currentResult
    currentResult -= enteredNumber ;
    createAndWriteOutput('-', initialResult, enteredNumber)
    writeToLOg('SUBSTRACT', initialResult, enteredNumber, currentResult)
}

function multiply() {
    const enteredNumber = getUserNumberInput();
    const initialResult = currentResult
    currentResult *= enteredNumber ;
    createAndWriteOutput('*', initialResult, enteredNumber)
    writeToLOg('MULTIPLY', initialResult, enteredNumber, currentResult)
}

function divide(){
    const enteredNumber = getUserNumberInput();
    const initialResult = currentResult
    currentResult /= enteredNumber ;
    createAndWriteOutput('/', initialResult, enteredNumber)
    writeToLOg('DIVIDE', initialResult, enteredNumber, currentResult)
}

addBtn.addEventListener('click', add);
subtractBtn.addEventListener('click', substract);
multiplyBtn.addEventListener('click', multiply);
divideBtn.addEventListener('click', divide);



// currentResult = (currentResult + 10) * 3 / 2 -1;

// let calculationDescription = `(${defaultResult}  + 10) * 3 / 2 -1`;

// currentResult = currentResult + 10;

